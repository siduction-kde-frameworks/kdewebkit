Source: kdewebkit
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake (>= 2.8.12),
               debhelper (>= 9),
               extra-cmake-modules (>= 5.11.0~),
               kio-dev (>= 5.11.0~),
               libkf5config-dev (>= 5.11.0~),
               libkf5coreaddons-dev (>= 5.11.0~),
               libkf5jobwidgets-dev (>= 5.11.0~),
               libkf5parts-dev (>= 5.11.0~),
               libkf5service-dev (>= 5.11.0~),
               libkf5wallet-dev (>= 5.11.0~),
               libqt5webkit5-dev (>= 5.2~),
               pkg-kde-tools (>= 0.15.15ubuntu1~),
               qtbase5-dev (>= 5.4)
Standards-Version: 3.9.6
XS-Testsuite: autopkgtest
Homepage: https://projects.kde.org/projects/frameworks/kdewebkit
Vcs-Browser: http://anonscm.debian.org/cgit/pkg-kde/frameworks/kdewebkit.git
Vcs-Git: git://anonscm.debian.org/pkg-kde/frameworks/kdewebkit.git

Package: libkf5webkit-dev
Section: libdevel
Architecture: any
Depends: libkf5webkit5 (= ${binary:Version}),
         libqt5webkit5-dev (>= 5.2~),
         ${misc:Depends}
Description: development files for kdewebkit
 The kdewebkit library sits on top of QtWebKit,
 providing KDE integration for icons, shortcuts,
 network operation (KIO), cookie management using
 (KCookieJar) and component embedding (KParts).
 .
 Contains development files for kdewebkit.

Package: libkf5webkit5
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: KDE Integration for QtWebKit.
 The kdewebkit library sits on top of QtWebKit,
 providing KDE integration for icons, shortcuts,
 network operation (KIO), cookie management using
 (KCookieJar) and component embedding (KParts).
Multi-Arch: same

Package: libkf5webkit5-dbg
Priority: extra
Section: debug
Architecture: any
Multi-Arch: same
Depends: libkf5webkit5 (= ${binary:Version}), ${misc:Depends}
Description: debug symbols for kdewebkit
 The kdewebkit library sits on top of QtWebKit,
 providing KDE integration for icons, shortcuts,
 network operation (KIO), cookie management using
 (KCookieJar) and component embedding (KParts).
 .
 Contains debug symbols for kdewebkit.
